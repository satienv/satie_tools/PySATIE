import math

from typing import Any, List, Tuple

SPEED_OF_SOUND = 0.34
ONE_OVER_SPEED_OF_SOUND = 1.0 / SPEED_OF_SOUND


def gain_from_distance(distance: float) -> float:
    """
    Calculate gain from distance

    :param distance:
    :return:
    """

    return math.log(distance_to_attenuation(distance)) * 20


def variable_delay(distance: float, factor: float) -> float:
    """
    Calculate delay from distance

    :param distance:
    :param factor:
    :return:
    """

    return math.pow(distance * ONE_OVER_SPEED_OF_SOUND, factor * 0.01)


def get_distance_freq(distance: float) -> float:
    """
    Calculate low pass cut-off
    Simulates air absorption based on distance.

    :param distance:
    :return:
    """
    return 100.0 + ((1.0 - (distance * ONE_OVER_SPEED_OF_SOUND * 0.00034002))**2 * 19900.0)


def distance_to_attenuation(distance: float) -> float:
    """
    A very crude distance to attenuation conversion.
    (we use the abs of the number you give us.)

    :param distance: Absolute distance in meters.
    :return:
    """

    d = abs(distance)
    return 1.0 if d <= 1.0 else 1.0 / d


def xyz_to_aed(xyz: List[float]) -> Tuple[float, float, float]:
    """
    Converts XYZ to AED.

    :param xyz: list of three floats
    :return: tuple of three floats
    """
    x, y, z = xyz

    distance = math.sqrt((x * x) + (y * y) + (z * z))
    azimuth = math.atan2(z, x) - math.pi / 2
    # put in range of [-pi, pi]
    if azimuth > math.pi:
        azimuth -= 2 * math.pi
    elif azimuth < -math.pi:
        azimuth += 2 * math.pi
    if distance > 0.0000001:
        elevation = math.asin(y / distance)
    else:
        elevation = 0.0

    # NOTE: -45 and -30 are hardcoded offsets for the dome at SAT
    return math.degrees(-azimuth), math.degrees(elevation), distance


def list_to_dict(items: List[Any]) -> List[dict]:
    """
    Take a list, presumambly in a [key_1, value_1, ... key+n, value_n]
    format and turn it into a Dictionary
    :param list:
    """
    res_dct = {items[i]: items[i + 1] for i in range(0, len(items), 2)}
    return res_dct
