import json
import logging
from pysatie.plugin import Plugin
from pysatie.property import Property

from typing import Any, Dict, List, TYPE_CHECKING

if TYPE_CHECKING:
    from pysatie.satie import Satie

logger = logging.getLogger(__name__)


class OSChandlers(object):
    """Define SATIE class aware OSC handlers."""

    def __init__(self, satie: 'Satie') -> None:
        """Initialize the class."""
        self._satie = satie

    def handle_plugins(self, *args) -> None:
        """Parse the json object containing plugins list.

        :param plugins: message containing raw JSON data
        :return:
        """
        plugins_raw = args[0]
        try:
            plugins_data = json.loads(plugins_raw)
        except json.JSONDecodeError as e:
            logger.error("Invalid plugin list json", e)
            return

        data_converted = self.convert_plugin_data(plugins_data)
        logger.debug(data_converted)
        self.remove_refreshed_plugins(data_converted)

        for datum in data_converted:
            if datum["name"] not in [p.name for p in self._satie.plugins]:
                self._satie.add_plugin(Plugin(self._satie, **datum))

        self._satie.emit('plugin_list_updated')

    def convert_plugin_data(self, payload: Dict) -> List[Dict]:
        """Convert plugins dictionary to Plugin class structure.

        Convert the dictionary of SATIE plugins data obtained via JSON to entries convenient
        for Plugin class.

        :param payload: Dict The SATIE plugins JSON converted to dictionary via json.loads
        :return: List  A list of dictionaries suitable for instantiating Plugin classes
        """
        plugins = []
        for category, plugs in payload.items():
            for plugin, data in plugs.items():
                name = plugin
                description = data['description']
                plugin_type = data['type']

                plugins.append({
                    "name": name,
                    "satie_type": plugin_type,
                    "category": category,
                    "description": description
                })
        return plugins

    def remove_refreshed_plugins(self, data):
        """Remove plugins from internal representation if not in data payload."""
        new_names = [n["name"] for n in data]
        old_names = [n.name for n in self._satie.plugins]
        for name in old_names:
            if name not in new_names:
                self._satie.remove_plugin(self._satie.get_plugin_by_name(name))

    def handle_plugin_properties(self, *args) -> None:
        """
        Parse plugin properties[ 1, nil, tree ].

        :param properties: Message containing raw JSON data
        :return:
        """
        properties_raw = args[0]
        try:
            props = json.loads(properties_raw)
        except Exception as e:
            logger.error(f"Invalid plugin properties json {e}")
            return

        try:
            for st, property_data in props.items():
                props = property_data['arguments']
                plugins = self._satie.get_plugins_by_type(property_data["srcName"])
            for plugin in plugins:
                plugin.properties = [
                        Property(
                            plugin,
                            name=prop['name'],
                            prop_type=prop['type'],
                            default_value=prop['value']
                        ) for prop in props
                    ]
        except AttributeError:
            pass

    def get_satie_configuration_json(self, *payload) -> None:
        """Handle reception of the JSON containing SATIE configuration."""
        self._satie.emit_data('satie_get_configuration', satie_configuration=payload[0])

    def get_satie_status_string(self, *payload) -> None:
        """Handle reception of the string containing SATIE status."""
        self._satie.emit_data('satie_get_status', satie_status=payload[0])

    def get_synth_parameters_json(self, *payload) -> None:
        """Handle reception of the JSON containing the parameters of a running synth."""
        self._satie.emit_data('satie_get_synth_parameters', synth_parameters=payload)

    def get_synth_value_json(self, payload) -> None:
        """
        Handle reception of the JSON containing the value of a given parameter of a running synth.
        """
        self._satie.emit_data('satie_get_synth_value', param_value=payload[0])

    def get_envelope(self, args) -> None:
        """Process anevelope message received."""
        node = self._satie.nodes.get(args[0][0])
        if node:
            node.analysis = ('analysis', args[0][1])

    # SATIE now sends /trigger/<node_name> <value>
    def get_trigger(self, args) -> None:
        """Process trigger message."""
        node = self._satie.nodes.get(args[0][0])
        if node:
            node.analysis = ('trigger', args[0][1])

    def signal_heartbeat(self) -> None:
        """Emit signal upon hearbeat message."""
        self._satie.emit('satie_heartbeat')

    def get_server_option(self, *vals) -> None:
        """
        Handle receiving server option

        :param vals: should always be a list.
        """
        data = {vals[0][0]: vals[0][1]}
        self._satie.emit_data('satie_query_result', **data)
