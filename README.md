# PySatie

PySatie is a Python module allowing for interactions with a [SATIE](https://gitlab.com/sat-metalab/SATIE) server.

Then install PySatie with `python3 -m pip install .` or for an editable (development) mode: `python3 -m pip install -e .` (executed in the root of PySatie directory)

## Status

It's a work in progress. It's probably broken in some ways but that is intended to change.

## API documentation

`doc` directory contains Sphinx documentation. You can install Sphinx with `sudo apt-get install python3-sphinx python3-sphinx-rtd-theme`
Then in the `doc` directory: `make html` and you can view it locally by opening `./doc/build/html/index.html` with a browser of your choice.

Additionally you will need to enable typehints in documentation:

``` bash
python3 -m pip install sphinx-autodoc-typehints
```

Lastly, make sure you have an updated version of the *sphinx-rtd-theme* installed. If you get a `Theme error` message while generating the documentation, you can run the following command to update your sphinx-rtd-theme:

```bash
python3 -m pip install sphinx-rtd-theme
```
